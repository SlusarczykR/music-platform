package com.slusarczykr.musicplatform.spotify.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.client.RestTemplate;

@Configuration
public class SpotifyAPIRestClient {

    @Bean
    @Primary
    public RestTemplate getRestTemplate(RestTemplateBuilder builder) {
        return builder.rootUri("some uri")
                .additionalInterceptors((request, body, execution) -> {
                    request.getHeaders().add("Bearer", "token");
                    return execution.execute(request, body);
                }).build();
    }
}
