package com.slusarczykr.musicplatform.spotify.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spotify.api")
@Getter
@Setter
public class SpotifyAPIProperties {

    String clientId;
    String clientSecret;
}
