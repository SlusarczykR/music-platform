package com.slusarczykr.musicplatform.spotify.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SpotifyService implements TrackService {

    private final RestTemplate restTemplate;

    @Override
    public List<String> getSimilarTracks(int trackId) {
        return new ArrayList<>();
    }
}
